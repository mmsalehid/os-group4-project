#include "types.h"
#include "stat.h"
#include "user.h"
// #include "defs.h"
//#include <stdio.h>

int
main(int argc, char *argv[])
{
    int oldVal;
    int newVal1 = atoi(argv[1]);
    int ESI ;
    printf(1, "newVal1 %d\n", newVal1);
    write(0, "resid", sizeof("resid"));
    asm volatile("movl %%esi, %0;"
        "movl %1, %%esi;"
        : "=r" (oldVal)
        : "r" (newVal1)
        : "%esi");
    printf(1, "oldval %d\n", oldVal);
    printf(1, "newVal1 %d\n", newVal1);
    count_num_of_digits();
    asm volatile("movl %1, %%esi;"
        "movl %%esi, %0;"
        : "=r" (ESI)
        : "r" (oldVal)
        : "%esi");
    printf(1, "ESI %d\n", ESI); 
    exit();
    return 0;
}