#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char** argv){
    int time1 = cmostime();
    int n = atoi(argv[1])*100;
    sleep_proc(n);
    int time2 = cmostime();
    printf(1,"sleep time: %d\n", time2 - time1);
    exit();
    return 0;
}