#include "types.h"
#include "stat.h"
#include "user.h"

int main()
{
	int childrenPids[4];
	int i;
	printf(1, "I am parent with pid %d\n", getpid());
	childrenPids[0] = fork();
	for (i = 0; i < 3; ++i)
	{
		int pid = childrenPids[i];
		if(pid == 0){
			printf(1, "my pid is %d and my parent pid is %d, i = %d\n", getpid(), getParentPid(), i);
			break;
		}	
		else{
			childrenPids[i + 1] = fork();
		}
	}
	if (i == 3 && childrenPids[3] == 0)
	{
		printf(1, "my pid is %d and my parent pid is %d, i = %d\n", getpid(), getParentPid(), i);
	}
	if(childrenPids[i] != 0){
		printf(1, "real result is : ");
		for (int j = 0; j < 4; ++j)
		{
			printf(1, "%d", childrenPids[j]);
		}
		printf(1, "\n");
		printf(1, "result of getChildren() systemcall is : ");
		getChildren(getpid());
		for (int j = 0; j < 4; ++j)
		{
			wait();
		}
	}	
	exit();
	return 0;
}

			//printf("I am a child and my pid is %d and pid of my parent is %d\n" ,getpid(), getParentPid());
