#include "types.h"
#include "stat.h"
#include "user.h"

int main(int argc, char** argv){
    int n = 1000000;
    if (fork() > 0){
        sleep_proc(n);
        wait();
    }
    else {
        sleep_proc(n);
    }

    exit();
    return 0;
}