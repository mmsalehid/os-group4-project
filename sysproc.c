#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return myproc()->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  release(&tickslock);
  return 0;
}

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

int find_num_of_digits (int number)
{
  int num_of_digits = 0, temp;
  temp = number;
  while (temp > 0)
  {
    temp = temp/10;
    num_of_digits ++;
  }
  return num_of_digits;
}


int
sys_count_num_of_digits(void)
{
  int number;
  struct proc *p = myproc();
  number = p->tf->esi;
  cprintf("esi = %d\n", number);
  int num_of_digits = find_num_of_digits(number);
  cprintf("num of digits = %d\n", num_of_digits);
  return 0;
  }

int
sys_sleep_proc(void){
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  release(&tickslock);
  sti();
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
  }
  return 0;
}

int
sys_cmostime(){
    struct rtcdate t1;
    cmostime(&t1);
    return t1.second + t1.minute * 60;
}

int
sys_getParentPid(void){
  struct proc *procParent = myproc()-> parent;
  return procParent->pid;
}


int
sys_getChildren(void){
  int procId;
  argint(0, &procId);
  findChildren(procId);
  // exit();
  return 0;
}

int sys_printQueue(void){
  //int noQ;
  // if(argint(0, &noQ) < 0)
  //   return -1;
  // if(noQ == 1){
  //   cprintf("Lottery queue : ");    
  //   for (int i = 0; i < noProcLev1; i++)
  //   {
  //     cprintf("%d ", queueLevel[noQ][i]);
  //   } 
  // }else if(noQ == 2){
  //   cprintf("HRRN : ");
  //   for (int i = 0; i < noProcLev2; i++)
  //   {
  //     cprintf("%d ", queueLevel[noQ][i]);
  //   } 
  // }else if(noQ == 3){
  //   cprintf("SRPF : ");
  //   for (int i = 0; i < noProcLev3; i++)
  //   {
  //     cprintf("%d ", queueLevel[noQ][i]);
  //   } 
  // } 
  // cprintf("/n");  
  return 0; 
}


int sys_assignLottery(){
  int pid, newLottery;
  if((argint(0, &pid)  < 0) | (argint(1, &newLottery) < 0))
    return -1;
  assignLot(pid, newLottery);
  return 0;
}
int sys_assignRemainingTime(){
  int pid, newRemainingTime;
  if((argint(0, &pid)  < 0) | (argint(1, &newRemainingTime) < 0))
    return -1;
  assignReTime(pid, newRemainingTime);
  return 0;
}

int sys_processAttribute(){
  int pid;
  if(argint(0, &pid) < 0)
    return -1;
  printProcAttr(pid);
  return 0;
}
int sys_printProcesses(){
  printAllProc();
  return 0;
}

int sys_changeLevelProc(){
  int pid, newLevel;
  if((argint(0, &pid)  < 0) | (argint(1, &newLevel) < 0))
    return -1;
  changeLevProc(pid, newLevel);
  return 0;
}

int sys_acquire(){
  struct spinlock* lk;
  
}